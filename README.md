# Inheritance Poetry

Inherit from Poem.py to:
- print your poem on the screen, 4 lines to a stanza.

Read Poem.py to learn about "self":
- "self" has been renamed "poem". Apparently this works.
