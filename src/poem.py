
class Poem:
    """
        Inherit from this class to:
        - print your poem on the screen, 4 lines to a stanza.
        - "self" has been renamed "poem". Apparently this works.
    """

    def __init__(poem, name="Unknown Poet"):
        poem.address = ""
        poem.title = ""
        poem.poet = name
        poem.lines_per_stanza = 0
        poem.lines = []

    def add_line(poem, line):
        poem.lines.append(line)

    def display(poem):
        indent = "    "
        print()
        print(indent + poem.title)
        print(indent + "by " + poem.poet)

        line_num = 0
        for line in poem.lines:
            if line_num % poem.lines_per_stanza == 0:
                # double-space between stanzas
                print()
            line_num += 1

            print(line)

        print()  # blank line after the poem
