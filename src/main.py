import poem


class Main(poem.Poem):
    def __init__(self, name="Unknown Poet"):
        super().__init__(name)
        self.poet = name


if __name__ == '__main__':
    poem = Main(name="Poeticus Covidus")
    poem.title = "My work-at-home poem"
    poem.lines_per_stanza = 4

    def writing_line(writes):
        return poem.actor + " found me at " + poem.address + " writing " + writes + "."

    poem.actor = "My wife"
    poem.address = "/work/Desktop"
    poem.add_line(writing_line("a poem"))
    poem.add_line(writing_line("sweet pill"))
    poem.add_line(writing_line("in rhythm"))
    poem.add_line(writing_line("so still"))

    poem.actor = "My son"
    poem.address = "/home/the_poet/Desktop"
    poem.add_line(writing_line("a stack poem"))
    poem.add_line(writing_line("in swill"))
    poem.add_line(writing_line("but solemn"))
    poem.add_line(writing_line("so still"))

    poem.actor = "No one"
    poem.address = "/home/bed"
    poem.add_line(writing_line("still still"))

    poem.display()
